import json
import re

from django.http import JsonResponse
from django.views import View
from django.views.decorators.csrf import csrf_exempt


class Verify(View):
    http_method_names = ['post']

    @csrf_exempt
    def post(self, request):
        ahv_pattern = re.compile(r'^\d{3}\.\d{4}\.\d{4}\.\d{2}$')

        if not request.body:
            return JsonResponse({"error": "Invalid"}, status=400)

        data = json.loads(request.body)
        if not ahv_pattern.match(data['ahvNumber']):
            return JsonResponse({"error": "Invalid"}, status=400)

        return JsonResponse({"correct": True}, status=200)
