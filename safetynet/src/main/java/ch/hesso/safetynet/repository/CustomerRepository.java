package ch.hesso.safetynet.repository;

import ch.hesso.safetynet.model.Customer;
import org.springframework.data.repository.CrudRepository;

/**
 * @author Ivan Kostanjevec
 */
public interface CustomerRepository extends CrudRepository<Customer, Integer> {
    Customer findCustomerByAhvNumber(String ahvNumber);
}
