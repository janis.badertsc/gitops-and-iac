package ch.hesso.safetynet.controller;

import ch.hesso.safetynet.model.InsuranceClaim;
import ch.hesso.safetynet.service.SafetyNetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author Ivan Kostanjevec
 */
@RestController
@RequestMapping(path = "/api/v1/")
public class SafetyNetController {

    private final SafetyNetService service;

    @Autowired
    public SafetyNetController(SafetyNetService service) {
        this.service = service;
    }

    @CrossOrigin
    @PostMapping(path = "send-form")
    public void handleClaim(@RequestBody InsuranceClaim claim) {
        service.handleInsuranceClaim(claim);
    }
}
