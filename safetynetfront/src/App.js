import Formular from './components/Formular';
import './App.css';

function App() {
  return (
      <div className="App">
        <div className="form-container">
          <Formular />
        </div>
      </div>
  );
}

export default App;
