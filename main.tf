terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "~> 3.0.2"
    }
  }
  backend "http" {
  }

  required_version = ">= 1.1.0"
}

provider "azurerm" {
  features {}
}

resource "azurerm_resource_group" "rg" {
  name     = "Terraform-${var.branch}"
  location = var.location
}

resource "azurerm_mssql_server" "server" {
  name                         = "git-ops-demo-db-server-${var.branch}-${var.user}"
  resource_group_name          = azurerm_resource_group.rg.name
  location                     = azurerm_resource_group.rg.location
  administrator_login          = var.db_admin_username
  administrator_login_password = var.db_admin_password
  version                      = "12.0"
}

resource "azurerm_mssql_firewall_rule" "allow_azure_services" {
  name                = "AllowAzureServices"
  server_id           = azurerm_mssql_server.server.id
  start_ip_address    = "0.0.0.0"
  end_ip_address      = "0.0.0.0"
}

resource "azurerm_mssql_database" "accounting_db" {
  name      = "accounting"
  server_id = azurerm_mssql_server.server.id
  max_size_gb = 1
  zone_redundant = false
  sku_name = "Basic"
  storage_account_type = "Local"
}

resource "azurerm_mssql_database" "client_db" {
  name      = "client"
  server_id = azurerm_mssql_server.server.id
  max_size_gb = 1
  zone_redundant = false
  sku_name = "Basic"
  storage_account_type = "Local"
}

resource "azurerm_container_group" "container" {
  name                = "git-ops_demo-${var.user}"
  location            = azurerm_resource_group.rg.location
  resource_group_name = azurerm_resource_group.rg.name
  ip_address_type     = "Public"
  os_type             = "Linux"
  restart_policy      = "Always"
  depends_on = [azurerm_mssql_database.accounting_db, azurerm_mssql_database.client_db]

  # AHV API Container
  container {
    name   = "ahv-api"
    image  = "registry.gitlab.com/${var.container_registry}/ahv-api:${var.branch}"
    cpu    = var.cpu
    memory = var.memory

    ports {
      port     = 1400
      protocol = "TCP"
    }
  }

  # Bank API Container
  container {
    name   = "bank-api"
    image  = "registry.gitlab.com/${var.container_registry}/bank-api:${var.branch}"
    cpu    = var.cpu
    memory = var.memory

    ports {
      port     = 8080
      protocol = "TCP"
    }
  }

  # Accounting API Container
  container {
    name   = "accounting-api"
    image  = "registry.gitlab.com/${var.container_registry}/accounting-api:${var.branch}"
    cpu    = var.cpu
    memory = var.memory

    ports {
      port     = 2500
      protocol = "TCP"
    }
  }

  # SafetyNet API Container
  container {
    name   = "safetynet-api"
    image  = "registry.gitlab.com/${var.container_registry}/safetynet-api:${var.branch}"
    cpu    = var.cpu
    memory = var.memory

    ports {
      port     = 5600
      protocol = "TCP"
    }
  }

  container {
    name   = "frontend"
    image  = "registry.gitlab.com/${var.container_registry}/frontend:${var.branch}"
    cpu    = var.cpu
    memory = var.memory

    ports {
      port     = 80
      protocol = "TCP"
    }
  }
}
