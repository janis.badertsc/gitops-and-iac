package ch.hesso.accounting.controller;

import ch.hesso.accounting.model.Claim;
import ch.hesso.accounting.service.AccountingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Ivan Kostanjevec
 */
@RestController
@RequestMapping(path = "/api/v2/")
public class AccountingController {

    private final AccountingService accountingService;

    @Autowired
    public AccountingController(AccountingService accountingService) {
        this.accountingService = accountingService;
    }

    @PostMapping(path = "/claim")
    public void processClaim(@RequestBody Claim claim) {
        accountingService.handleClaim(claim);
    }
}
